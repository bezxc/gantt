import React, { SyntheticEvent, useRef, useEffect } from "react";
import styles from "./horizontal.module.css";

export const HorizontalScroll: React.FC<{
  scroll: number;
  svgWidth: number;
  taskListWidth: number;
  rtl: boolean;
  onScroll: (event: SyntheticEvent<HTMLDivElement>) => void;
}> = ({ scroll, svgWidth, onScroll }) => {
  const scrollRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (scrollRef.current) {
      scrollRef.current.scrollLeft = scroll;
    }
  }, [scroll]);

  return (
    <div className={styles.scrollWrapper} onScroll={onScroll} ref={scrollRef}>
      <div style={{ width: svgWidth }} className={styles.scroll} />
    </div>
  );
};
